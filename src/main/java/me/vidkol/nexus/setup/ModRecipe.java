package me.vidkol.nexus.setup;

import me.vidkol.nexus.Nexus;
import me.vidkol.nexus.crafting.recipie.PressingRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraftforge.fml.RegistryObject;

public class ModRecipe {

    public static class Types {
        public static final IRecipeType<PressingRecipe> PRESSING = IRecipeType.register(Nexus._MODID + "pressing");

    }

    public static class Serializers {
        public static final RegistryObject<IRecipeSerializer<?>> PRESSING = Registration.RECIPE_SERIALIZER.register("pressing", PressingRecipe.Serializer::new);
    }

    static void register() {}
}
